# Article Feed

Want to view in production?

Visit [Article Feed](https://article-feed.vercel.app/)

or run locally ...

## How to set up the app

- Clone the repository
- Run `yarn` to install all of the dependencies needed for the project to function

## How to start the app

- Run `yarn start`
- The app will open and you will see the news feed :)

## Below you can find the tech stack that was used in this project

- React
- GraphQL
- Apollo
- Scss / BEM name spacing
- Jest / React Testing Library

## Lets have a look at the screens!

### Mobile

![Mobile](readmeAssets/mobile.png)

### Tablet

![Tablet](readmeAssets/tablet.png)

### Desktop

![Desktop](readmeAssets/desktop.png)
