import { Icons } from './components';
import { LandingPage } from './pages';

import './styles/utils.scss';
import './styles/theme.scss';

const App = () => {
  return (
    <div className="App">
      <Icons />
      <LandingPage />
    </div>
  );
};

export default App;
