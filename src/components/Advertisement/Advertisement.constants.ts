const ADVERTISEMENT_IMAGES = Object.freeze({
  mobile: 'Landscape Nexus',
  desktop: 'Google Nexus',
});

const advertisementConstants = Object.freeze({
  ADVERTISEMENT_IMAGES: ADVERTISEMENT_IMAGES,
});

export { advertisementConstants };
