import { useEffect, useState } from 'react';

import { ApolloError } from '@apollo/react-hooks';
import isempty from 'lodash.isempty';

import { ButtonIconOnly } from '../ButtonIconOnly';
import { Preloader } from '../Preloader';

import { advertisementConstants } from './Advertisement.constants';

import { useGetAdvertisementImageByTitle, useMediaQuery } from '../../hooks';

import './Advertisement.scss';

const Advertisement = () => {
  const { ADVERTISEMENT_IMAGES } = advertisementConstants;

  const [advertisementImageDescription, setAdvertisementImageDescription] = useState<string>();
  const [advertisementImageUrl, setAdvertisementImageUrl] = useState<string>();
  const [isAdvertisementVisible, setIsAdvertisementVisible] = useState<boolean>(true);

  const isMobile = useMediaQuery({ size: 'small' });
  const advertisementTitle = isMobile ? 'mobile' : 'desktop';

  const { handleGetAdvertisementImageByTitle, isLoading: isHandleGetAdvertisementImageByTitle } = useGetAdvertisementImageByTitle({
    onCompleted: ({ advertisementImage: image }) => {
      if (isempty(image)) {
        return;
      }

      const {
        description,
        image: { url },
      } = image;

      setAdvertisementImageDescription(description);
      setAdvertisementImageUrl(url);
    },
    onError: (error?: ApolloError) => {
      if (!error) {
        return;
      }

      console.error(error);
    },
  });

  const handleCloseAdvertisement = () => {
    setIsAdvertisementVisible(false);
  };

  useEffect(() => {
    handleGetAdvertisementImageByTitle({
      title: ADVERTISEMENT_IMAGES[advertisementTitle],
    });
  }, [advertisementTitle]);

  if (!isAdvertisementVisible) {
    return null;
  }

  const isLoading = isHandleGetAdvertisementImageByTitle;

  return (
    <>
      <Preloader isLoading={Boolean(isLoading)} />
      <div className="c-advertisement">
        <ButtonIconOnly
          additionalClassNames="c-advertisement__close-icon"
          onClick={handleCloseAdvertisement}
          type="button"
          iconId="close-icon"
          text="close advertisement"
        />

        <img className="c-advertisement__image" src={advertisementImageUrl} alt={advertisementImageDescription} />
      </div>
    </>
  );
};

export { Advertisement };
