const DEFAULT_PAGE_SIZE = 6;
const TEASER_CATEGORY = 'royal-news';

const articleCardListConstants = Object.freeze({
  TEASER_CATEGORY: TEASER_CATEGORY,
  DEFAULT_PAGE_SIZE: DEFAULT_PAGE_SIZE,
});

export { articleCardListConstants };
