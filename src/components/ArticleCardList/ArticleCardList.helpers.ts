import { articleCardListConstants } from './ArticleCardList.constants';

export const getMoreItems = (pageSize: number): number => {
  const { DEFAULT_PAGE_SIZE } = articleCardListConstants;
  const size: number = pageSize + DEFAULT_PAGE_SIZE;

  return size;
};
