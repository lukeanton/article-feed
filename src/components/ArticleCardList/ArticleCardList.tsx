import { useState, useEffect } from 'react';

import { ApolloError } from '@apollo/react-hooks';
import cx from 'classnames';
import isempty from 'lodash.isempty';

import { Button } from '../Button';
import { Preloader } from '../Preloader';

import { articleCardListConstants } from './ArticleCardList.constants';
import { getMoreItems } from './ArticleCardList.helpers';

import { ITeaserData, useGetTeasersByCategory } from '../../hooks';

import './ArticleCardList.scss';

//This is the component that displays the list of articles
// Notice that this component is limited to only 1 type of article ?
// This can be solved by adding a prop called articleType or teaserCategory, this will make the component pure and re-usable.
const ArticleCardList = () => {
  const { DEFAULT_PAGE_SIZE, TEASER_CATEGORY } = articleCardListConstants;

  // Ordered state
  const [articleTeasers, setArticleTeasers] = useState<ITeaserData[]>();
  const [isPagination, setIsPaginationDisabled] = useState<boolean>(false);
  const [pageSize, setPageSize] = useState<number>(DEFAULT_PAGE_SIZE);
  const [totalTeasers, setTotalTeasers] = useState<number>();

  // Teaser hook with onComplete and onError handling
  // Data has been reshaped for readability within hook
  const { handleGetTeasersByCategory, isLoading: isHandleGetTeasersByCategory = false } = useGetTeasersByCategory({
    onCompleted: ({ teasers }) => {
      if (isempty(teasers)) {
        return;
      }

      const { data, totalCount } = teasers;

      setArticleTeasers(data);
      setTotalTeasers(totalCount);
    },
    onError: (error?: ApolloError) => {
      if (!error) {
        return;
      }

      console.error(error);
    },
  });

  // Calling the api when the component renders
  useEffect(() => {
    handleGetTeasersByCategory({ category: TEASER_CATEGORY });
  }, []);

  useEffect(() => {
    if (!articleTeasers || !totalTeasers) {
      return;
    }

    setIsPaginationDisabled(totalTeasers <= pageSize);
  }, [pageSize, articleTeasers, totalTeasers]);

  // A single loading boolean that contains all api loading booleans
  const isLoading = isHandleGetTeasersByCategory;

  return (
    <>
      <Preloader isLoading={isLoading} />
      <div className="c-article-card-list">
        {articleTeasers &&
          articleTeasers
            .slice(0, pageSize)
            .map(({ id: uniqueTeaserId, contentTitle, contentImageUrl, contentSummary, contentImageAltText }, index) => {
              const isFirstIndex = Number(index) === 0;

              const articleCardClassNames = cx('c-article-card', {
                'c-article-card--is-full-width': isFirstIndex,
              });

              return (
                <article key={uniqueTeaserId} className={articleCardClassNames}>
                  <div className="c-article-card__image-container">
                    <img alt={contentImageAltText} className="c-article-card__image" src={contentImageUrl} />
                  </div>
                  <div>
                    <h2 className="c-article-card__title">{contentTitle}</h2>
                    <span className="c-article-card__summary">{contentSummary}</span>
                  </div>
                </article>
              );
            })}
        {articleTeasers && (
          <div className="c-article-card-list__load-more-containter">
            <Button text="Load More" isDisabled={isPagination} onClick={() => setPageSize(getMoreItems(pageSize))}></Button>
          </div>
        )}
      </div>
    </>
  );
};

export { ArticleCardList };
