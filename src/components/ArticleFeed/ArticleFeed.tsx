import { Advertisement } from '../Advertisement';
import { ArticleCardList } from '../ArticleCardList';
import './ArticleFeed.scss';

// This is the ArticleFeed component that will render a list of Articles and display an advertisement
const ArticleFeed = () => {
  return (
    <div className="c-article-feed l-container-max-width">
      <ArticleCardList />
      <Advertisement />
    </div>
  );
};

export { ArticleFeed };
