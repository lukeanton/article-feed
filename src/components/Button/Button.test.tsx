import { render, screen, fireEvent } from '@testing-library/react';

import '@testing-library/jest-dom';
import { Button } from './Button';

import { ButtonVariantType, ButtonType } from '../../types';

export type ButtonVariantTestType = [ButtonVariantType, string];
export type ButtonTestType = [ButtonType, string];

describe('Button', () => {
  const text = 'Click me!';

  it('Should display the Button component in the document', () => {
    render(<Button onClick={() => null} text={text} />);

    const button = screen.getByTestId('qa-button');

    expect(button).toBeInTheDocument();
  });

  it('Should display the Button component with the expected text in the document', () => {
    render(<Button onClick={() => null} text={text} />);

    const buttonText = screen.getByText(text);

    expect(buttonText).toBeInTheDocument();
  });

  describe('isDisabled prop', () => {
    describe('When the isDisabled prop is set to true', () => {
      it('Should disable the Button', () => {
        render(<Button onClick={() => null} text={text} isDisabled />);

        const button = screen.getByTestId('qa-button');

        expect(button).toBeDisabled();
      });
    });

    describe('When the isDisabled prop is set to false', () => {
      it('Should not disable the Button', () => {
        render(<Button onClick={() => null} text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).not.toBeDisabled();
      });
    });
  });

  describe('onClick prop', () => {
    describe('When the onClick prop is set', () => {
      const onClickSpy = jest.fn();

      describe('When Button is clicked', () => {
        it('Should call the onClick function 1 time', () => {
          render(<Button onClick={onClickSpy} text={text} />);

          const button = screen.getByTestId('qa-button');

          fireEvent.click(button);

          expect(onClickSpy).toBeCalledTimes(1);
        });
      });
    });

    describe('When the onClick prop is not set', () => {
      it('Should not have the onClick attribute', () => {
        render(<Button onClick={() => null} text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).not.toHaveAttribute('onClick');
      });
    });
  });

  describe('type prop', () => {
    const buttonTypes: ButtonTestType[] = [
      ['button', 'button'],
      ['submit', 'submit'],
      ['reset', 'reset'],
    ];

    it.each(buttonTypes)('When the type prop is set to %s, it should have the attribute type %s', (type, expected) => {
      render(<Button onClick={() => null} text={text} type={type} />);

      const button = screen.getByTestId('qa-button');

      expect(button).toHaveAttribute('type', expected);
    });

    describe('When the type is not set', () => {
      it('Should have the attribute type button', () => {
        render(<Button onClick={() => null} text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).toHaveAttribute('type', 'button');
      });
    });
  });

  describe('variant prop', () => {
    const variants: ButtonVariantTestType[] = [
      ['inverse', 'c-button--inverse'],
      ['primary', 'c-button--primary'],
    ];

    it.each(variants)('When the variant prop is set to %s, it should have the %s css class', (variant, expected) => {
      render(<Button onClick={() => null} text={text} variant={variant} />);

      const button = screen.getByTestId('qa-button');

      expect(button).toHaveClass(expected);
    });

    describe('When the variant prop is not set', () => {
      it('Should have the c-button--primary css class', () => {
        render(<Button onClick={() => null} text={text} />);

        const button = screen.getByTestId('qa-button');

        expect(button).toHaveClass('c-button--primary');
      });
    });
  });
});
