import cx from 'classnames';

import { ButtonProps } from './Button.types';

import './Button.scss';

const Button: React.FC<ButtonProps> = ({
  additionalClassNames,
  text,
  type = 'button',
  variant = 'primary',
  isDisabled = false,
  onClick,
}) => {
  const buttonClassNames = cx('c-button', `c-button--${variant}`, additionalClassNames);

  return (
    <button data-testid="qa-button" className={buttonClassNames} disabled={isDisabled} type={type} onClick={onClick}>
      {text}
    </button>
  );
};

export { Button };
