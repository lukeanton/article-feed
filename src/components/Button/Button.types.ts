import { IAdditionalClassNames, IIsDisabled, IOnClickButton } from '../../interfaces';
import { ButtonType, ButtonVariantType } from '../../types';

export interface ButtonProps extends IAdditionalClassNames, IIsDisabled, IOnClickButton {
  /**
   * Specify the text content of the button
   */
  text: string;
  /**
   * Specify the type of button you would like to display
   */
  type?: ButtonType;
  /**
   * Specify the variant of button you would like to display
   */
  variant?: ButtonVariantType;
}
