import { IAdditionalClassNames, IIsDisabled, IOnClickButton } from '../../interfaces';
import { ButtonVariantType, IconIdType } from '../../types';

export interface ButtonIconOnlyProps extends IAdditionalClassNames, IIsDisabled, IOnClickButton {
  /**
   * Specify an icon id to display as the apps logo
   */
  iconId?: IconIdType;
  /**
   * Specify if the svg icon has a border
   */
  isIconBorderVisible?: boolean;
  /**
   * Specify if the svg icon is selected so its styles can be updated
   */
  isIconSelected?: boolean;
  /**
   * The hidden text for the icon which will be read for screen readers
   */
  text: string;
  /**
   * The URL that the hyperlink points to.
   */
  type?: 'button' | 'submit';
  /**
   * Specify the variant of button you would like to display
   */
  variant?: ButtonVariantType;
}
