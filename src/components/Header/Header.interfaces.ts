import { IIcon, INavItem } from '../../interfaces';
import { IconIdType } from '../../types';

export interface HeaderProps {
  headerLogoId: IconIdType;
  navItems: INavItem[];
  utilityIcons: IIcon[];
}
