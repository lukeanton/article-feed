import { Icon } from '../Icon';

import { HeaderProps } from './Header.interfaces';

import './Header.scss';

const Header: React.FC<HeaderProps> = ({ headerLogoId, navItems, utilityIcons }) => {
  return (
    <header className="c-header">
      <div className="c-header-wrapper l-container-max-width">
        <Icon additionalClassNames="c-header__logo" iconId={headerLogoId} />
        <nav aria-label="header navigation items" className="c-header__navigation">
          {navItems.map(({ title, url }) => (
            <a className="c-header__navigation__anchor" href={url}>
              {title}
            </a>
          ))}
        </nav>
        {utilityIcons.map(({ iconId }) => (
          <Icon additionalClassNames="c-header__utility-icons" iconId={iconId} />
        ))}
      </div>
    </header>
  );
};

export { Header };
