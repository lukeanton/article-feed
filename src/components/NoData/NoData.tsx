import { NoDataProps } from './NoData.interfaces';

import './NoData.scss';

const NoData: React.FC<NoDataProps> = ({ message = 'No data' }) => {
  return <div className="c-no-data">{message}</div>;
};

export { NoData };
