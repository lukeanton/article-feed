import { IIcon, INavItem } from '../../interfaces';
import { IconIdType } from '../../types';

const HEADER_NAV_ITEMS: INavItem[] = [
  { title: 'lifestyle', url: '/lifestyle' },
  { title: 'travel', url: '/travel' },
  { title: 'news', url: '/news' },
  { title: 'health', url: '/health' },
];

const SEARCH_ICON: IconIdType = 'search-icon';
const TITLE_PREFIX = 'Womans Weekly';
const UTILITY_ICONS: IIcon[] = [{ iconId: SEARCH_ICON }];
const WOMANS_WEEKLY_ICON: IconIdType = 'womans-weekly-icon';

const pageLayoutConstants = Object.freeze({
  TITLE_PREFIX: TITLE_PREFIX,
  HEADER_NAV_ITEMS: HEADER_NAV_ITEMS,
  UTILITY_ICONS: UTILITY_ICONS,
  WOMANS_WEEKLY_ICON: WOMANS_WEEKLY_ICON,
});

export { pageLayoutConstants };
