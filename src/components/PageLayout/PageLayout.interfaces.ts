export interface IMeta {
  description: string;
}

export interface PageLayoutProps {
  meta?: IMeta;
  pageName: string;
  pageTitle: string;
}
