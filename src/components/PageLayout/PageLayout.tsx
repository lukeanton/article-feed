import { Helmet } from 'react-helmet';

import { Header } from '../Header';

import { pageLayoutConstants } from './PageLayout.constants';
import { IMeta, PageLayoutProps } from './PageLayout.interfaces';

const PageLayout: React.FC<PageLayoutProps> = ({ children, meta, pageName, pageTitle }) => {
  const { HEADER_NAV_ITEMS, UTILITY_ICONS, WOMANS_WEEKLY_ICON } = pageLayoutConstants;

  const { description = 'Page description' } = meta ?? ({} as IMeta);

  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
        <meta content={description} name={pageName} />
      </Helmet>
      <Header headerLogoId={WOMANS_WEEKLY_ICON} navItems={HEADER_NAV_ITEMS} utilityIcons={UTILITY_ICONS} />
      <main className="h-header-offset-y">{children}</main>
    </>
  );
};

export { PageLayout };
