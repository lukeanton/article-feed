import cx from 'classnames';

import { Icon } from '../Icon';

import './Preloader.scss';

import { PreloaderProps } from './Preloader.types';

const Preloader: React.FC<PreloaderProps> = ({ additionalClassNames, iconId = 'spinner-icon', isLoading = false }) => {
  if (!isLoading) {
    return null;
  }

  const preloaderClassNames = cx('c-preloader-overlay', additionalClassNames);

  return (
    <div className={preloaderClassNames} role="status">
      <div className="c-preloader__wrapper">
        <Icon additionalClassNames="c-preloader__spinner" iconId={iconId} />
      </div>
    </div>
  );
};

export { Preloader };
