export interface TitleBlockProps {
  description: string;
  title: string;
}
