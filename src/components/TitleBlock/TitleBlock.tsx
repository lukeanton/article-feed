import { TitleBlockProps } from './TitleBlock.interfaces';

import './TitleBlock.scss';

// The TitleBlock component is used for pages with a title and description.
// This assures that each page has a h1 and description with the aria-labbeledby attributes for screen readers
const TitleBlock: React.FC<TitleBlockProps> = ({ title, description }) => {
  return (
    <div className="c-title-block l-container-max-width">
      <h1 aria-labelledby="Title" className="c-title-block__title">
        {title}
      </h1>
      <span className="c-title-block__description" aria-labelledby="Description">
        {description}
      </span>
    </div>
  );
};

export { TitleBlock };
