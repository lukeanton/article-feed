const MOBILE_QUERY = 'only screen and (max-width: 680px)';
const ROYAL_NEWS_CATEGORY_ID = 'royal-news';

const globalConstants = Object.freeze({
  ROYAL_NEWS_CATEGORY_ID: ROYAL_NEWS_CATEGORY_ID,
  MOBILE_QUERY: MOBILE_QUERY,
});

export { globalConstants, MOBILE_QUERY };
