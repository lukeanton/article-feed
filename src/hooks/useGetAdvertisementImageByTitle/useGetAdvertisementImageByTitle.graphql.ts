import { gql } from '@apollo/react-hooks';

export const GET_ADVERTISEMENT_BY_TITLE_QUERY = gql`
  query getAdvertisementImageByTitle($title: String) {
    getAdvertisementImageByTitle: advertisementImages(where: { title: $title }) {
      title
      description
      image {
        url
      }
    }
  }
`;
