import { ApolloError, DocumentNode } from '@apollo/react-hooks';

export interface IApolloResponseVariables {
  error?: ApolloError;
  isLoading?: boolean;
}

export interface IOnCompleted<T> {
  onCompleted?: (data: T) => void;
}
export interface IOnError<T> {
  onError?: (error: T) => void;
}

export interface IDBImage {
  url: string;
}

export interface IDBAdvertisementImage {
  description: string;
  image: IDBImage;
  title: string;
}

export interface IHandleGetAdvertisementImageByTitleParams extends IGetAdvertisementImageByTitleQueryVariables {}

export interface IGetAdvertisementImageByTitleQueryGraphQLResponse {
  geAdvertisementImageByTitle: IDBAdvertisementImage[];
}

export interface IGetAdvertisementImageByTitleQueryVariables {
  title: IDBAdvertisementImage['title'];
}

export interface IGetAdvertisementImageByTitleOnCompletedResponse {
  advertisementImage: IDBAdvertisementImage;
}

export interface IUseGetAdvertisementImageByTitle extends IApolloResponseVariables {
  handleGetAdvertisementImageByTitle: (params: IHandleGetAdvertisementImageByTitleParams) => void;
}

export interface IUseGetAdvertisementImageByTitleOptions
  extends IOnCompleted<IGetAdvertisementImageByTitleOnCompletedResponse>,
    IOnError<ApolloError> {
  query?: DocumentNode;
}
