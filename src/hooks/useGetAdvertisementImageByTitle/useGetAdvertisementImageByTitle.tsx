/* eslint-disable @typescript-eslint/no-misused-promises */
import { useLazyQuery } from '@apollo/client';

import { GET_ADVERTISEMENT_BY_TITLE_QUERY } from './useGetAdvertisementImageByTitle.graphql';
import {
  IHandleGetAdvertisementImageByTitleParams,
  IUseGetAdvertisementImageByTitle,
  IUseGetAdvertisementImageByTitleOptions,
} from './useGetAdvertisementImageByTitle.interfaces';

import { client } from '../../middleware/client';

const useGetAdvertisementImageByTitle = (options?: IUseGetAdvertisementImageByTitleOptions): IUseGetAdvertisementImageByTitle => {
  const { query, onCompleted, onError } = options ?? ({} as IUseGetAdvertisementImageByTitleOptions);

  const [executeGetAdvertisementImageByTitle, { loading: isLoading }] = useLazyQuery(query ?? GET_ADVERTISEMENT_BY_TITLE_QUERY, {
    client,
    fetchPolicy: 'cache-and-network',
    onCompleted: (data) => {
      if (!onCompleted) {
        return;
      }

      const { getAdvertisementImageByTitle } = data;

      const [advertisementImage] = getAdvertisementImageByTitle;

      onCompleted({
        advertisementImage,
      });
    },
    onError,
  });

  const handleGetAdvertisementImageByTitle = async ({ title }: IHandleGetAdvertisementImageByTitleParams) => {
    await executeGetAdvertisementImageByTitle({
      variables: {
        title,
      },
    });
  };

  return {
    handleGetAdvertisementImageByTitle,
    isLoading,
  };
};

export { useGetAdvertisementImageByTitle };
