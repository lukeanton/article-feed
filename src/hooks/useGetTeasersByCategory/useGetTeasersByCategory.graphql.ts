import { gql } from '@apollo/react-hooks';

export const GET_TEASERS_BY_CATEGORY_QUERY = gql`
  query getTeasersByCategory($category: String) {
    getTeasersByCategory: teasers(where: { category: $category }) {
      teaserJSON
    }
  }
`;
