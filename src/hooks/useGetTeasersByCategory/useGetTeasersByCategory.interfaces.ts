import { ApolloError, DocumentNode } from '@apollo/react-hooks';

import { IApolloResponseVariables } from '../useGetAdvertisementImageByTitle';

import { IOnCompleted, IOnError } from '../../interfaces';
export interface ITeaserData {
  contentImageAltText: string;
  contentImageUrl: string;
  contentSummary: string;
  contentTitle: string;
  id: string;
  pageDateCreated: string;
  source: string;
  url: string;
}

export interface ITeaserJson {
  data: ITeaserData[];
  totalCount: number;
}

export interface IHandleGetTeasersByCategoryParams extends IGetTeasersByCategoryQueryVariables {}

export interface IGetTeasersByCategoryQueryVariables {
  category: string;
}

export interface IGetTeasersByCategoryOnCompletedResponse {
  teasers: ITeaserJson;
}

export interface IUseGetTeasersByCategory extends IApolloResponseVariables {
  handleGetTeasersByCategory: (params: IHandleGetTeasersByCategoryParams) => void;
}

export interface IUseGetTeasersByCategoryOptions extends IOnCompleted<IGetTeasersByCategoryOnCompletedResponse>, IOnError<ApolloError> {
  query?: DocumentNode;
}
