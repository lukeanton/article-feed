import { useLazyQuery } from '@apollo/client';

import { GET_TEASERS_BY_CATEGORY_QUERY } from './useGetTeasersByCategory.graphql';
import {
  IHandleGetTeasersByCategoryParams,
  IUseGetTeasersByCategory,
  IUseGetTeasersByCategoryOptions,
} from './useGetTeasersByCategory.interfaces';

import { client } from '../../middleware/client';

const useGetTeasersByCategory = (options?: IUseGetTeasersByCategoryOptions): IUseGetTeasersByCategory => {
  const { query, onCompleted, onError } = options ?? ({} as IUseGetTeasersByCategoryOptions);

  const [executeGetTeasersByCategory, { loading: isLoading }] = useLazyQuery(query ?? GET_TEASERS_BY_CATEGORY_QUERY, {
    client,
    onCompleted: (data) => {
      if (!onCompleted) {
        return;
      }

      const { getTeasersByCategory } = data;

      const [teaserData] = getTeasersByCategory;
      const { teaserJSON: teasers } = teaserData;

      onCompleted({
        teasers,
      });
    },
    onError,
  });

  const handleGetTeasersByCategory = async ({ category }: IHandleGetTeasersByCategoryParams) => {
    await executeGetTeasersByCategory({
      variables: {
        category,
      },
    });
  };

  return {
    handleGetTeasersByCategory,
    isLoading,
  };
};

export { useGetTeasersByCategory };
