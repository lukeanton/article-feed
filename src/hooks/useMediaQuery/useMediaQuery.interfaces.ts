export type ScreenSizeType = 'small' | 'medium' | 'large';

export interface IUseMediaQuery {
  size: ScreenSizeType;
}
