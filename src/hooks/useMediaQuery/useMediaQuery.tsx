import { useState, useEffect } from 'react';

import { SCREEN_SIZES } from './useMediaQuery.constants';
import { IUseMediaQuery, ScreenSizeType } from './useMediaQuery.interfaces';

const useMediaQuery = ({ size }: IUseMediaQuery) => {
  const [hasMatches, setHasMatches] = useState<boolean>(false);
  const [query] = useState<ScreenSizeType>(SCREEN_SIZES[size] as ScreenSizeType);

  useEffect(() => {
    const media = window.matchMedia(String(query));
    const handleSetMatches = () => setHasMatches(media.matches);

    if (media.matches !== hasMatches) {
      handleSetMatches();
    }

    media.addEventListener('change', handleSetMatches);
    return () => media.removeEventListener('change', handleSetMatches);
  }, [hasMatches, query]);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setHasMatches(window.matchMedia(String(query)).matches);
    }
  }, [query]);

  return hasMatches;
};

export { useMediaQuery };
