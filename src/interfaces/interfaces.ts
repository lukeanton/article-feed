import { SyntheticEvent } from 'react';

export interface IAdditionalClassNames {
  /**
   * Specify additional class names that apply to the parent node
   */
  additionalClassNames?: string;
}

export interface IIsDisabled {
  /**
   * The boolean value that decides wether the element is disabled
   */
  isDisabled?: boolean;
}

export interface INavItem {
  /**
   * The title of the nav item
   */
  title: string;
  /**
   * The title of the nav item
   */
  url: string;
}

export interface IIcon {
  /**
   * Specify the id of the icon
   */
  iconId: IconIdType;
}
export interface IOnClickButton {
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => void;
}

// GQL

import { ApolloError } from '@apollo/client';
import { IconIdType } from '../types';

export interface IApolloResponseVariables {
  error?: ApolloError;
  isLoading?: boolean;
}

export interface IOnCompleted<T> {
  onCompleted?: (data: T) => void;
}

export interface IOnError<T> {
  onError?: (error: T) => void;
}
