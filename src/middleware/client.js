import { ApolloClient, HttpLink, ApolloLink, InMemoryCache, concat } from '@apollo/client';

import { AUTH_TOKEN, CONTENT_URL } from './dummyvariables';

const httpLink = new HttpLink({ uri: CONTENT_URL });
const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      gcmsStage: 'PUBLISHED',
      authorization: `${String(AUTH_TOKEN)}`,
    },
  }));

  return forward(operation);
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink),
});

export { client };
