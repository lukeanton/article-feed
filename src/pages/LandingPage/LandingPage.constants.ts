const LANDING_PAGE_DESCRIPTION = 'Royal family news feed';
const LANDING_PAGE_META = 'Royal family news feed';
const LANDING_PAGE_NAME = 'Article feed page';
const LANDING_PAGE_TITLE = 'Article feed';

const landingPageConstants = Object.freeze({
  LANDING_PAGE_DESCRIPTION: LANDING_PAGE_DESCRIPTION,
  LANDING_PAGE_META: LANDING_PAGE_META,
  LANDING_PAGE_NAME: LANDING_PAGE_NAME,
  LANDING_PAGE_TITLE: LANDING_PAGE_TITLE,
});

export { landingPageConstants };
