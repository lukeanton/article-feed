import { landingPageConstants } from './LandingPage.constants';

import { PageLayout, ArticleFeed, TitleBlock } from '../../components';

const LandingPage = () => {
  // All pages that use hardcoded variables have been pulled out into a constants file for readability and cleanliness
  const { LANDING_PAGE_NAME, LANDING_PAGE_DESCRIPTION, LANDING_PAGE_TITLE, LANDING_PAGE_META } = landingPageConstants;

  //Each page requires the PageLayout component that accepts mandatory meta and page title for seo optimization
  return (
    <PageLayout meta={{ description: LANDING_PAGE_META }} pageName={LANDING_PAGE_NAME} pageTitle={LANDING_PAGE_TITLE}>
      <TitleBlock title={LANDING_PAGE_TITLE} description={LANDING_PAGE_DESCRIPTION} />
      <ArticleFeed />
    </PageLayout>
  );
};

export { LandingPage };
