export type ButtonType = 'button' | 'submit' | 'reset';

export type ButtonVariantType = 'inverse' | 'primary';

export type IconIdType = 'close-icon' | 'search-icon' | 'spinner-icon' | 'womans-weekly-icon';
